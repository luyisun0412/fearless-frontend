import React, {useEffect, useState} from "react"
function PresentationForm(){
    const [conferences, setConferences] = useState([])
    const [name, setName] = useState("")
    const [email, setEmail] = useState("")
    const [companyName, setCompanyName] = useState("")
    const [title, setTitle] = useState("")
    const [synopsis, setSynopsis] = useState("")
    const [conference, setConference] = useState("")

    const handleName = (event) =>{
      setName(event.target.value)
    }

    const handleEmail = (event) => {
      setEmail(event.target.value)
    }

    const handleCompanyName = (event) =>{
      setCompanyName(event.target.value)
    }

    const handleTitle = (event) => {
      setTitle(event.target.value)
    }

    const handleSynopsis = (event) =>{
      setSynopsis(event.target.value)
    }

    const handleConference = (event) =>{
      setConference(event.target.value)
    }

    const handleSubmit = async (event) =>{
      event.preventDefault()

      const data = {}
      data.presenter_name = name
      data.presenter_email = email
      data.company_name = companyName
      data.title = title
      data.synopsis = synopsis


      const url = `http://localhost:8000${conference}presentations/`
      const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers:{
          "Content-Type": "application/json"
        }
      }
      try{
      const response = await fetch(url, fetchConfig)
      if (response.ok){
        const newPresenter = await response.json()
        setName("")
        setEmail("")
        setCompanyName("")
        setConference("")
        setSynopsis("")
        setTitle("")
      }
    }catch(error) {
      console.error(error)
    }

    }

    const fetchData = async () =>{
        const url = "http://localhost:8000/api/conferences/"

        try{
            const response = await fetch(url)
            if (response.ok) {
                const data = await response.json()
                setConferences(data.conferences)
            }
        }catch(error){
            console.error(error)
        }


    }

    useEffect(()=>{
        fetchData()
    }, [])

    return (
        <>
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange={handleName} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control" value={name}/>
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEmail} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control" value={email}/>
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCompanyName} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control" value={companyName} />
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleTitle} placeholder="Title" required type="text" name="title" id="title" className="form-control" value={title}/>
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea onChange={handleSynopsis} className="form-control" id="synopsis" rows="3" name="synopsis" value={synopsis}></textarea>
              </div>
              <div className="mb-3">
                <select onChange={handleConference} required name="conference" id="conference" className="form-select" value={conference}>
                  <option value="">Choose a conference</option>
                  {conferences.map(conference => {
                    return (
                      <option value={conference.href} key={conference.href}> {conference.name} </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>

        </>
    )

}


export default PresentationForm
