import React, {useState, useEffect} from "react"
function AttendeeList(){
  const[attendees, setAttendees] = useState([])

  const fetchData = async () => {
    try {
      const response = await fetch('http://localhost:8001/api/attendees/');

      if(response.ok){
        const data = await response.json()
        setAttendees(data.attendees)
        // root.render(
        //   <React.StrictMode>
        //     <App attendees={data.attendees} />
        //   </React.StrictMode>
        // );
      }
    } catch(error){
      console.error(error)
    }
  }

  useEffect(() => {
    fetchData()
  }, [])

  const handleDelete = async (href)=>{
    const request = await fetch(`http://localhost:8001/${href}`, {method: "DELETE"})
    // const resp=await request.json()
    // console.log(resp)
    fetchData()
  }

 return (
    <>
      <table className="table">
        <thead>
          <tr>
            <th className="col-6" scope="col">Name</th>
            <th  className="col-6" scope="col">Conference</th>
          </tr>
        </thead>
        <tbody>
          {attendees.map(attendee => {
            return (
              <tr className="table-secondary" key={attendee.href}>
                <td > {attendee.name} </td>
                <td > {attendee.conference}</td>
                <td ><button onClick={()=>handleDelete(attendee.href)} className="btn btn-danger">Delete</button></td>
              </tr>
            )
          })}

        </tbody>
      </table>
    </>
 )
}

export default AttendeeList
