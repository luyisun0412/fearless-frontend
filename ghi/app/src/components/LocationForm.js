import React, { useState, useEffect } from "react";
import {useNavigate} from "react-router-dom"

function LocationForm() {
  const [states, setStates] = useState([]);
  // const [name, setName] = useState("");
  // const [city, setCity] = useState("");
  // const [roomCount, setRoomCount] = useState(0);
  // const [state, setState] = useState("");
  const [formData, setFormData] = useState({
    name:"",
    city:"",
    room_count:"",
    state:""
  })
  let navigate = useNavigate()

  const handleFormChange = (event) =>{
    const inputName = event.target.name;
    const value = event.target.value
    setFormData({...formData, [inputName]:value})
  }

  // const handleNameChange = (event) => {
  //   const value = event.target.value;
  //   setName(value);
  // };

  // const handleCityChange = (event) => {
  //   setCity(event.target.value);
  // };

  // const handleRoomCountChange = (event) => {
  //   setRoomCount(event.target.value);
  // };

  // const handleStateChange = (event) => {
  //   setState(event.target.value);
  // };

  const handleSubmit = async (event) => {
    event.preventDefault();
    // const data = {};

    // data.room_count = roomCount;
    // data.name = name;
    // data.state = state;
    // data.city = city;

    const locationUrl = "http://localhost:8000/api/locations/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      // navigate("/attendees")

      // const newLocation = await response.json();
      // setName("");
      // setRoomCount("");
      // setCity("");
      // setState("");
      setFormData({
        name:"",
        city:"",
        room_count:"",
        state:""
      })
    }
  };

  const fetchData = async () => {
    const url = "http://localhost:8000/api/states/";
    try {
      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setStates(data.states);
      }
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new location</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input
                  onChange={handleFormChange}
                  placeholder="Name"
                  required
                  type="text"
                  id="name"
                  className="form-control"
                  name="name"
                  value={formData.name}
                />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleFormChange}
                  placeholder="Room count"
                  required
                  type="number"
                  id="room_count"
                  className="form-control"
                  name="room_count"
                  value={formData.room_count}
                />
                <label htmlFor="room_count">Room count</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleFormChange}
                  placeholder="City"
                  required
                  type="text"
                  id="city"
                  className="form-control"
                  name="city"
                  value={formData.city}
                />
                <label htmlFor="city">City</label>
              </div>
              <div className="mb-3">
                <select
                  onChange={handleFormChange}
                  required
                  id="state"
                  className="form-select"
                  name="state"
                  value={formData.state}
                >
                  <option value="">Choose a state</option>
                  {states.map((state) => {
                    return (
                      <option
                        value={state.abbreviation}
                        key={state.abbreviation}
                      >
                        {state.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}

export default LocationForm;
