import React, {useState, useEffect} from "react"
function AttendeeSignUp() {

    const [conferences, setConferences] = useState([])
    const [name, setName] = useState("")
    const [email, setEmail] = useState("")
    const [conference, setConference] = useState("")
    const [condition, setCondition] = useState(false)

    const handleSubmit = async (event) =>{
        event.preventDefault()

        const data = {}

        data.name = name
        data.email = email
        data.conference = conference

        const attendeeUrl = `http://localhost:8001${conference}attendees/`

        const fetchConfig = {
            method:"POST",
            body:JSON.stringify(data),
            headers:{
                "Content-Type": "application/json"
            }
        }

        try{
            const response = await fetch(attendeeUrl, fetchConfig)
            if(response.ok){
                const newAttendee = await response.json()
                setCondition(true)

                setName("");
                setEmail("");
                setConference("");


            }
        }catch(error){
            console.error(error)
        }
    }

    let spinnerClasses = conferences.length >0? '' : 'd-none';
    let dropdownClasses = conferences.length >0? 'form-select': 'form-select d-none';
    let successPage = {
        color:"#163020",
        backgroundColor: "#AFC8AD"
    }
    // if (conferences.length > 0) {
    //   spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
    //   dropdownClasses = 'form-select';
    // }

    const handleName = (event) =>{
        setName(event.target.value)
    }

    const handleEmail = (event) =>{
        setEmail(event.target.value)
    }

    const handleConference = (event) =>{
        setConference(event.target.value)
    }



    const fetchData = async() => {
        const url = "http://localhost:8000/api/conferences/"

        try{
            const response = await fetch(url)

            if(response.ok){
                const data = await response.json()
                setConferences(data.conferences)

            }
        }catch(error){
            console.error(error)
        }
    }

    useEffect(()=>{
        fetchData();
    }, [])



  return (
    <>

        <div className="row">
            <div className="col-3">
            <div className="shadow p-2 mt-5">
                <div className={spinnerClasses}>
                    <img src="/logo.svg" alt="conference-logo" />
                </div>
            </div>
            </div>
        {condition ? <div className="col-9">
        <div className="form offset col-11">
        <div className="shadow p-2 mt-5">
            <div style={successPage}>Congratulations! You're all signed up!</div>

        </div>
        </div>
        </div>:

            <div className="col-9">
            <div className="form offset col-11">
                <div className="shadow p-2 mt-5">
                <h1> It's Conference Time!</h1>
                <p>Please choose which conference you'd like to attend.</p>
                <form id="attendee-signup-form" onSubmit={handleSubmit}>
                    <div className="form-group col-md-12">
                        <label htmlFor="conference"></label>
                        <select id="conference" className={dropdownClasses} required onChange={handleConference} value={conference}>
                        <option value="">Choose a conference</option>

                        {conferences.map((conference) => {
                            return (
                                <option value={conference.href} key={conference.href}>{conference.name}</option>
                            )
                        })}
                        </select>
                    </div>

                    <div className="form-group col-md-12 mt-3">
                        {/* <p className="mt-6">Now, tell us about yourself.</p> */}
                        Now, tell us about yourself.
                    </div>

                    <div className="row">
                        <div className="form-group col-md-6">
                        <label htmlFor="name"></label>
                        <input
                            type="text"
                            className="form-control"
                            id="name"
                            placeholder="Your full name"
                            onChange={handleName}
                            value={name}
                        />
                        </div>
                        <div className="form-group col-md-6">
                        <label htmlFor="email"></label>
                        <input
                            type="email"
                            className="form-control"
                            id="email"
                            placeholder="Your email address"
                            onChange={handleEmail}
                            value={email}
                        />
                        </div>
                    </div>

                    <div className="form-group row mt-2">
                        <div className="col-md-10">
                        <button className="btn btn-primary">I'm going!</button>
                        </div>
                    </div>
            </form>
            </div>
          </div>
        </div>
        }
        </div>
    </>
  );
}

export default AttendeeSignUp;
