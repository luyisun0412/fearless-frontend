import React, { useState, useEffect } from "react";

function ConferenceForm(prop) {
  const [locations, setLocations] = useState([]);
  const [name, setName] = useState("");
  const [starts, setStarts] = useState("");
  const [ends, setEnds] = useState("");
  const [description, setDescription] = useState("");
  const [maxPresentation, setMaxPresentation] = useState(0);
  const [maxAttendees, setMaxAttendees] = useState(0);
  const [location, setLocation] = useState("");

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};

    data.name = name;
    data.starts = starts;
    data.ends = ends;
    data.description = description;
    data.max_presentations = maxPresentation;
    data.max_attendees = maxAttendees;
    data.location = location;

    const conferenceUrl = "http://localhost:8000/api/conferences/";

    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      Content: {
        "Content-Type": "application/json",
      },
    };
    try {
      const response = await fetch(conferenceUrl, fetchConfig);
      if (response.ok) {
        const newConference = await response.json();

        setName("");
        setStarts("");
        setEnds("");
        setDescription("");
        setMaxAttendees(0);
        setMaxPresentation(0);
        setLocation("");
      }
    } catch (error) {
      console.log(error);
    }
  };

  const fetchData = async () => {
    const url = "http://localhost:8000/api/locations/";
    try {
      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setLocations(data.locations);
      }
    } catch (error) {
      console.error(error);
    }
  };
  const handleName = (event) => {
    setName(event.target.value);
  };

  const handleStarts = (event) => {
    setStarts(event.target.value);
  };

  const handleEnds = (event) => {
    setEnds(event.target.value);
  };

  const handleDescription = (event) => {
    setDescription(event.target.value);
  };

  const handleMaxAttendees = (event) => {
    setMaxAttendees(event.target.value);
  };

  const handleMaxPresentations = (event) => {
    setMaxPresentation(event.target.value);
  };

  const handleLocation = (event) => {
    setLocation(event.target.value);
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form id="create-location-form" onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input
                  placeholder="Name"
                  required
                  type="text"
                  id="name"
                  className="form-control"
                  name="name"
                  onChange={handleName}
                  value={name}
                />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  placeholder="mm/dd/yyyy"
                  required
                  type="date"
                  id="starts"
                  className="form-control"
                  name="starts"
                  onChange={handleStarts}
                  value={starts}
                />
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  placeholder="mm/dd/yyyy"
                  required
                  type="date"
                  id="ends"
                  className="form-control"
                  name="ends"
                  onChange={handleEnds}
                  value={ends}
                />
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="mb-3">
                <label htmlFor="description">Description</label>
                <textarea
                  required
                  id="description"
                  className="form-control"
                  name="description"
                  rows="3"
                  onChange={handleDescription}
                  value={description}
                ></textarea>
              </div>
              <div className="form-floating mb-3">
                <input
                  placeholder="Maximum presentations"
                  required
                  type="number"
                  id="max_presentations"
                  className="form-control"
                  name="max_presentations"
                  onChange={handleMaxPresentations}
                  value={maxPresentation}
                />
                <label htmlFor="max_presentations">Maximum presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  placeholder="Maximum attendees"
                  required
                  type="number"
                  id="max_attendees"
                  className="form-control"
                  name="max_attendees"
                  onChange={handleMaxAttendees}
                  value={maxAttendees}
                />
                <label htmlFor="max_attendees">Maximum attendees</label>
              </div>
              <div className="mb-3">
                <select
                  required
                  id="location"
                  className="form-select"
                  name="location"
                  onChange={handleLocation}
                  value={location}
                >
                  <option value="">Choose a Location</option>
                  {locations.map((location) => {
                    return (
                      <option value={location.id} key={location.id}>
                        {location.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}

export default ConferenceForm;
