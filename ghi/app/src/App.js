import Nav from "./components/Nav";
import React, {useEffect, useState} from "react"
import {Routes, Route} from "react-router-dom"
import AttendeeList from "./components/AttendeesList";
import LocationForm from "./components/LocationForm";
import ConferenceForm from "./components/ConferenceForm";
import AttendeeSignUp from "./components/AttendeeSignUp";
import Home from "./components/Home";
import PresentationForm from "./components/PresentationForm";



function App() {


  return (
    <>
      <Nav />
      <div className="container">
      <Routes>
        <Route index element={<Home />} />
        <Route path="attendees" element={ <AttendeeList/>}>
          <Route path="new" element={<AttendeeSignUp />} />
        </Route>
        <Route path="conferences" >
          <Route path="new" element={ <ConferenceForm /> } />
        </Route>
        <Route path="locations" >
          <Route path="new" element={<LocationForm />}/>
          <Route index element={<Home />} />
        </Route>
        <Route path="presentations" >
          <Route path="new" element={<PresentationForm />}/>
        </Route>
      </Routes>
      </div>
    </>
  );
}



export default App;
